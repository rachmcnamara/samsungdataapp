/**
 * Created by rachelmcnamara on 3/28/17.
 */
import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { Route } from 'react-router-dom';
import App from '../views/App'
import VideoDetail from '../views/VideoDetail'
import ShareDetail from '../views/ShareDetail'
import VideoPlayerContainer from './VideoPlayerContainer'
import reducers from '../reducers'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import {createLogger} from 'redux-logger'
import createHistory from 'history/createHashHistory'
import {routerReducer, routerMiddleware, ConnectedRouter } from 'react-router-redux'
import {messageService} from '../services/MessageService'

const loggerMiddleware = createLogger()
const history =  createHistory()

// Build the middleware for intercepting and dispatching navigation actions
const routerMid = routerMiddleware(history)

const store = createStore(
    reducers,
    routerReducer,
    applyMiddleware(
        thunkMiddleware,
        routerMid,
        loggerMiddleware
    )
)

export default class Root extends Component
{
  constructor(props)
  {
    super(props)
    messageService.init(store.dispatch)
  }

  render() {
    return (
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <div>
              <Route path='/home' component={App} />
              <Route path='/Detail' component={VideoDetail} />
              <Route path='/Share' component={ShareDetail}/>
              <Route path='/VideoPlayerContainer' component={VideoPlayerContainer}/>
            </div>
          </ConnectedRouter>
        </Provider>
    )
  }
}
