/**
 * Created by rachelmcnamara on 3/28/17.
 */

import React, {Component} from 'react'

export default class MusicList extends Component{
  constructor(){
    super()
  }

  render(){

    var listStyle = {
      display:"flex",
      flexDirection:"row",
      overflow:"auto",
      listStyle:"none"
    }

    var itemStyle = {
      padding:10,
      fontSize:24,
      width:375
    }

    return (
        <ul style={listStyle}>
          {this.props.music.map((item) =>
            <li key={item.content.id} style={itemStyle}>
              <img src={item.content.poster} onClick={()=>this.props.handleClick(item.url)} width="375" height="500"/>
              <div>
                <p>{item.content.title}</p>
              </div>
            </li>
          )}
        </ul>
    )
  }
}