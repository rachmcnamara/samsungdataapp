/**
 * Created by rachelmcnamara on 4/5/17.
 */
/**
 * Created by rachelmcnamara on 3/28/17.
 */

import React, {Component} from 'react'

export default class ImageList extends Component{
  constructor(){
    super()
  }

  render(){

    var listStyle = {
      display:"flex",
      flexDirection:"row",
      overflow:"auto",
      listStyle:"none"
    }

    var itemStyle = {
      padding:10,
      fontSize:24
    }

    return (
        <ul style={listStyle}>
          {this.props.images.map((image) =>
              <li key={image.content.id} style={itemStyle}>
                <img src={image.content.link} onClick={()=>this.props.handleClick(image.url)} width={"240"}/>
              </li>
          )}
        </ul>
    )
  }
}