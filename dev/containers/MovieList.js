/**
 * Created by rachelmcnamara on 3/28/17.
 */

import React, {Component} from 'react'

export default class MovieList extends Component{
  constructor(){
    super()
  }

  render(){

    var listStyle = {
      display:"flex",
      flexDirection:"row",
      overflow:"auto",
      listStyle:"none"
    }

    var itemStyle = {
      padding:10,
      fontSize:24
    }

    return (
        <ul style={listStyle}>
          {this.props.movies.map((movie) =>
            <li key={movie.content.id} style={itemStyle}>
              <img onClick={()=>this.props.handleClick(movie.url)} src={movie.content.poster.url} width={movie.content.poster.width} height={movie.content.poster.height}/>
              <div>
                <p>{movie.content.title}</p>
                <p>Source:{movie.content.source}</p>
              </div>
            </li>
          )}
        </ul>
    )
  }
}