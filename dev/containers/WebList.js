/**
 * Created by rachelmcnamara on 3/28/17.
 */

import React, {Component} from 'react'

String.prototype.trim = function (length) {
  return this.length > length ? this.substring(0, length) + "..." : this;
}


export default class WebList extends Component{
  constructor(){
    super()
  }

  render(){

    var listStyle = {
      display:"flex",
      flexDirection:"row",
      flexWrap:"wrap",
      overflow:"auto",
      listStyle:"none"
    }

    var itemStyle = {
      padding:10,
      fontSize:24,
      flexBasis:400,
      textAlign:"center"
    }

    var titleStyle = {
      textColor:"blue",
      fontSize:34,
      fontWeight:"bold",
      width:375
    }

    var linkStyle = {
      fontSize:14,
      width:375,
      fontColor:"blue",
      fontStyle:"underline"
    }

    var descStyle = {
      width:375
    }

    return (
        <ul style={listStyle}>
          {this.props.web.map((webItem) =>
            <li onClick={()=>this.props.handleClick(webItem.url)} key={webItem.content.cacheid} style={itemStyle}>
              <div>
                <p style={titleStyle}>{webItem.content.title}</p>
                <p style={descStyle}>{webItem.content.snippet.trim(50)}</p>
                <p style={linkStyle}>{webItem.url}>{webItem.url}</p>
              </div>
            </li>
          )}
        </ul>
    )
  }
}