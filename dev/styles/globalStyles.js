/**
 * Created by rachelmcnamara on 3/30/17.
 */
export var titleStyle = {
  padding:10,
  margin:10,
  fontFamily:"Tahoma",
  fontSize:32,
  textAlign:"center"
};

export var pageStyle = {
  textAlign:"center"
}

export var buttonStyle={
  width:250,
  height:50,
  fontSize:32,
  fontFamily:"Tahoma"
}

export var inputStyle = {
  margin:10,
  padding:10,
  fontSize:32
}