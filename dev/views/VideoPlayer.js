/**
 * Created by velopedev on 4/17/17.
 */
import React, {Component, PropTypes} from "react"
import {connect} from 'react-redux'
import {playVideo, pauseVideo, muteVideo, rewindVideo, ffVideo} from '../actions/videoPlayerActions'
import {PLAYER_STATES} from '../reducers/VideoPlayerReducer'

class VideoPlayer extends Component
{
    constructor(props) {

        super(props)

        this.maxRate = 32

        this.isPlaying = false
        this.handlePlay = this.handlePlay.bind(this)
        this.handleMute = this.handleMute.bind(this)
        this.handleSeek = this.handleSeek.bind(this)
        this.handleRewind = this.handleRewind.bind(this)
        this.handleFF = this.handleFF.bind(this)
        this.handleUpdate = this.handleUpdate.bind(this)
        this.handleVolume = this.handleVolume.bind(this)
    }

    componentDidMount()
    {
        this.player = document.getElementById("video")
        this.player.addEventListener("timeupdate", this.handleUpdate)
    }

    handleVolume()
    {
        this.player.volume = this.volumeBar.value/100
    }

    handleUpdate()
    {
        var value = (100 / this.player.duration) * this.player.currentTime;
        this.seekBar.value = value;
    }

    handleRewind()
    {
        //needs a different implementation
        var playerState = this.props.videoPlayerState.playbackState
        var currentRate = playerState == PLAYER_STATES.REW?this.props.videoPlayerState.rate:1
        var nextSpeed = (currentRate * 2)

        if(nextSpeed <= this.maxRate)
        {
            this.props.rewindVideo(nextSpeed)
            this.player.playbackRate = nextSpeed * -1
        }
    }

    handleFF()
    {
        var playerState = this.props.videoPlayerState.playbackState
        var currentRate = playerState == PLAYER_STATES.FF?this.props.videoPlayerState.rate:1
        var nextSpeed = currentRate * 2

        if(nextSpeed <= this.maxRate)
        {
            this.props.ffVideo(nextSpeed)
            this.player.playbackRate = nextSpeed
        }
    }

    handleSeek()
    {
        var time = this.player.duration * (this.seekBar.value / 100);
        this.player.currentTime = time;
    }

    handleMute()
    {
        if(this.player.muted == true)
        {
            this.player.muted = false
            this.props.muteVideo(false)
        }
        else
        {
            this.player.muted = true
            this.props.muteVideo(true)
        }
    }

    handlePlay()
    {

        if(this.player.paused == true)
        {
            this.player.playbackRate = 1
            this.player.play()
            this.props.playVideo()
        }
        else
        {
            this.player.pause()
            this.props.pauseVideo()
        }
    }

    render()
    {
        var playPauseCommand = this.props.videoPlayerState.playbackState == PLAYER_STATES.PLAYING?"||":">"
        var ff = ">>"
        var rewind = "<<"

        return(
            <div id="video-container">
                <video id="video" width="640" height="365" src={this.props.src} type={this.props.type}/>
                <div id="video-controls">
                    <div>
                        <input type="range" id="seek-bar" value="0" onChange={this.handleSeek} ref={(a) => this.seekBar = a}/>
                        <input type="range" id="volume-bar" min="0" max="100" value="50" step="5" onChange={this.handleVolume} ref={(a) => this.volumeBar = a}/>
                    </div>
                    <div>
                        <button type="button" id="rewind" onClick={this.handleRewind}>{rewind}</button>
                        <button type="button" id="play-pause" onClick={this.handlePlay}>{playPauseCommand}</button>
                        <button type="button" id="ff" onClick={this.handleFF}>{ff}</button>
                        <button type="button" id="mute" onClick={this.handleMute}>Mute</button>
                    </div>
                </div>
            </div>
        )
    }
}

VideoPlayer.propTypes = {
    isPlaying:PropTypes.bool
}

const mapStateToProps = (state) => {

    return {
        videoPlayerState:state.videoPlayerState
    };
};

const mapDispatchToProps = (dispatch) => {
    return{
        playVideo:()=>dispatch(playVideo()),
        pauseVideo:()=>dispatch(pauseVideo()),
        muteVideo:(isMuted)=>dispatch(muteVideo(isMuted)),
        ffVideo:(rate)=>dispatch(ffVideo(rate)),
        rewindVideo:(rate)=>dispatch(rewindVideo(rate))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(VideoPlayer);
