/**
 * Created by rachelmcnamara on 3/17/17.
 */
import React, {Component, PropTypes} from "react"
import {globalSearch} from '../actions/messageServiceActions'
import {setCurrentTab, setCurrentURL} from '../actions/UIStateActions'
import {connect} from 'react-redux'
import MovieList from '../containers/MovieList'
import WebList from '../containers/WebList'
import MusicList from '../containers/MusicList'
import ImageList from '../containers/ImageList'
import * as globalStyles from '../styles/globalStyles'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { push } from 'react-router-redux'

class App extends Component
{
  constructor(props)
  {
    super(props)

    this.handleSearch = this.handleSearch.bind(this)
    this.handleTab = this.handleTab.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  handleSearch(e)
  {
    var searchTerm = this._inputElement.value

    this.props.globalSearch(searchTerm)

    if (e)
      e.preventDefault();
  }

  handleTab(currIndex, lastIndex)
  {
    this.props.setCurrentTab(currIndex)
  }

  handleClick(url)
  {
    this.props.setCurrentURL(url)
    this.props.goToPage('/Detail')
  }

  render()
  {
    var tabStyle = {
      fontSize:24
    }

    return (

        <div style={globalStyles.pageStyle}>
          <p style={globalStyles.titleStyle}>Samsung test client</p>
          <form onSubmit={this.handleSearch}>
            <input type="submit" style={globalStyles.buttonStyle} value="Search"/>
            <input type="text" style={globalStyles.inputStyle} ref={(a) => this._inputElement = a}/>
          </form>

          <Tabs
              onSelect={this.handleTab}
              selectedIndex={this.props.currentTab}
          >
            <TabList>
              <Tab style={tabStyle}>All</Tab>
              <Tab style={tabStyle}>Web</Tab>
              <Tab style={tabStyle}>Movies & TV</Tab>
              <Tab style={tabStyle}>Images</Tab>
              <Tab style={tabStyle}>Music</Tab>
              <Tab style={tabStyle}>Video</Tab>
            </TabList>

            <TabPanel>

            </TabPanel>

            <TabPanel>
              <WebList web={this.props.search.web} handleClick={this.handleClick}/>
            </TabPanel>

            <TabPanel>
              <MovieList movies={this.props.search.media} handleClick={this.handleClick}/>
            </TabPanel>

            <TabPanel>
              <ImageList images={this.props.search.images} handleClick={this.handleClick}/>
            </TabPanel>

            <TabPanel>
              <MusicList music={this.props.search.music} handleClick={this.handleClick}/>
            </TabPanel>

            <TabPanel>
              <MovieList movies={this.props.search.videos} handleClick={this.handleClick}/>
            </TabPanel>
          </Tabs>

        </div>
    )
  }
}

App.propTypes = {
  search:PropTypes.object
}

const mapStateToProps = (state) => {

  return {
    currentTab:state.currentTab,
    search:state.search
  };
};

const mapDispatchToProps = (dispatch) => {
  return{
    goToPage:(url) => dispatch(push(url)),
    setCurrentTab:(index) => dispatch(setCurrentTab(index)),
    setCurrentURL:(url) => dispatch(setCurrentURL(url)),
    globalSearch:(searchTerm)=>dispatch(globalSearch(searchTerm))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
