/**
 * Created by rachelmcnamara on 4/6/17.
 */
import React, {Component, PropTypes} from "react"
import {connect} from 'react-redux'

class ShareDetail extends Component
{
  constructor(props)
  {
    super(props)
  }

  render(){

    var iFrameStyle = {
      display:"block",
      margin:"auto"
    }
    var include = this.props.msg.msg?this.props.msg.msg.url:""

    return(
        <div>
            <iframe style={iFrameStyle} src={include} width="1800" height="1400"></iframe>
        </div>
    )
  }
}

ShareDetail.propTypes = {
  msg:PropTypes.object
}


const mapStateToProps = (state) => {

  return {
    msg:state.newMessage
  };
};

export default connect(mapStateToProps, null)(ShareDetail);