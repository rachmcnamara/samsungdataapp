/**
 * Created by rachelmcnamara on 4/6/17.
 */
import React, {Component, PropTypes} from "react"
import {connect} from 'react-redux'
import {messageService} from '../services/MessageService'

class VideoDetail extends Component
{
  constructor(props)
  {
    super(props)
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(){
    messageService.sendMessage('share', {url:this.props.url})
  }

  render(){

    var iFrameStyle = {
      display:"block",
      margin:"auto"
    }

    var buttonStyle = {
      display:"block",
      marginTop:20,
      marginLeft:"auto",
      marginRight:"auto",
      padding:20,
      fontSize:24,
      backgroundColor:"#E8E8E8"
    }

    return(
        <div>
          <iframe style={iFrameStyle} src={this.props.url} width="1800" height="1400"></iframe>
          <button style={buttonStyle} type="button" height="50"  width="70" onClick={this.handleClick}>Share</button>
        </div>
    )
  }
}

const mapStateToProps = (state) => {

  return {
    url:state.currentURL
  };
};

const mapDispatchToProps = (dispatch) => {
  return{
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VideoDetail);