/**
 * Created by rachelmcnamara on 4/7/17.
 */
import io from 'socket.io-client'
import {receiveMessage, receiveSearchResults} from '../actions/messageServiceActions'


class MessageService
{
  init(dispatch)
  {
    this.socket = io("http://localhost:3000")

    this.socket.on("share", function (msg) {
        dispatch(receiveMessage({"type":"share", msg:msg}))
    })

    this.socket.on("movieResults", function(movies){
        dispatch(receiveSearchResults("media",movies))
    })

  	this.socket.on("musicResults", function(music){
	    dispatch(receiveSearchResults("music",music))
  	})

    this.socket.on("webResults", function(web){
        dispatch(receiveSearchResults("web",web))
    })

    this.socket.on("videoResults", function(web){
        dispatch(receiveSearchResults("videos",web))
    })

    this.socket.on("imageResults", function(images){
        dispatch(receiveSearchResults("images",images))
    })

    this.socket.on("error", function(e){
      console.log("Error connecting " + e)
    })
  }

  sendMessage(msg, data) {
    this.socket.emit(msg, data)
  }
}

export let messageService = new MessageService()


