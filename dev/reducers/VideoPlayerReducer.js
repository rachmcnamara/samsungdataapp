/**
 * Created by velopedev on 4/17/17.
 */
import {PLAY_VIDEO, PAUSE_VIDEO, MUTE_VIDEO, FF_VIDEO, REWIND_VIDEO } from '../actions/videoPlayerActions'

export var PLAYER_STATES = {
    PLAYING:1,
    PAUSED:2,
    FF:3,
    REW:4
}

export function handleVideoPlayer(state = {rate:1}, action)
{
    switch (action.type){
        case PLAY_VIDEO:
            return Object.assign({}, state, {
                playbackState: PLAYER_STATES.PLAYING,
                rate:1
        })
        case PAUSE_VIDEO:
            return Object.assign({}, state, {
                playbackState:PLAYER_STATES.PAUSED,
                isPlaying: action.payload.isPlaying
            })
        case MUTE_VIDEO:
            return Object.assign({}, state, {
                isMuted: action.payload.isMuted
            })
        case FF_VIDEO:
            return Object.assign({}, state, {
                playbackState: PLAYER_STATES.FF,
                rate:action.payload.rate
            })
        case REWIND_VIDEO:
            return Object.assign({}, state, {
                playbackState: PLAYER_STATES.REW,
                rate:action.payload.rate
            })
        default:
            return state
    }
}