/**
 * Created by rachelmcnamara on 4/4/17.
 */

import {SET_CURRENT_TAB, SET_CURRENT_URL} from '../actions/UIStateActions'

export function setCurrentTab(state=0, action){
  switch (action.type){
    case SET_CURRENT_TAB:
      return action.payload.index
    default:
      return state
  }
}

export function setCurrentUrl(state=0, action){
  switch (action.type){
    case SET_CURRENT_URL:
      return action.payload.url
    default:
      return state
  }
}