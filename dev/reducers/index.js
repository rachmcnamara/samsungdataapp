/**
 * Created by rachelmcnamara on 3/29/17.
 */
import { combineReducers } from 'redux';

import {setCurrentTab, setCurrentUrl} from './UIStateReducer'
import {receiveMessage} from './messageServiceReducer'
import {handleVideoPlayer} from './VideoPlayerReducer'
import {receiveSearchResults} from './searchReducer'

export default combineReducers({
  currentTab:setCurrentTab,
  currentURL:setCurrentUrl,
  newMessage:receiveMessage,
  videoPlayerState:handleVideoPlayer,
  search:receiveSearchResults
})