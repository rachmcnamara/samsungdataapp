/**
 * Created by rachelmcnamara on 4/12/17.
 */
import {RECEIVE_MESSAGE} from '../actions/messageServiceActions'

export function receiveMessage(state = {}, action)
{
  switch (action.type){
    case RECEIVE_MESSAGE:
      return action.payload.msg
    default:
      return state
  }
}
