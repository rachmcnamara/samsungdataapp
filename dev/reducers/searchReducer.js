/**
 * Created by velopedev on 4/18/17.
 */
import {RECEIVE_SEARCH_RESULTS, CLEAR_SEARCH_RESULTS} from '../actions/messageServiceActions'
import update from 'immutability-helper';

export function receiveSearchResults(state = {media:[], music:[], web:[], videos:[], images:[]}, action)
{
    switch (action.type){
        case RECEIVE_SEARCH_RESULTS:
            if(state[action.payload.key].length == 0)
            {
                var obj = {}
                obj[action.payload.key] = action.payload.data

				return Object.assign({}, state, obj)
            }
            else
            {
                var existing = state[action.payload.key]
                var obj = {}

                existing.push.apply(existing, action.payload.data)
                obj[action.payload.key] = existing

                return Object.assign({}, state, obj)
            }
        case CLEAR_SEARCH_RESULTS:
            return {media:[], music:[], web:[], videos:[], images:[]}
        default:
            return state
    }
}
