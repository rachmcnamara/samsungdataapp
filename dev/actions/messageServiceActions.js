/**
 * Created by rachelmcnamara on 4/10/17.
 */

import {messageService} from '../services/MessageService'

export const RECEIVE_MESSAGE = "receiveMessage"
export const RECEIVE_SEARCH_RESULTS = "receiveSearchResults"
export const CLEAR_SEARCH_RESULTS = "clearSearchResults"

export function receiveMessage(msg)
{
  return {
    type:RECEIVE_MESSAGE,
    payload:{
      msg:msg
    }
  }
}

export function clearSearchResults(msg)
{
	return {
		type:CLEAR_SEARCH_RESULTS
	}
}

export function globalSearch(searchTerm) {
	return (dispatch, state) => {
		dispatch(clearSearchResults())
		messageService.sendMessage('globalSearch', searchTerm)
	}
}

export function receiveSearchResults(key, data)
{
    return{
        type:RECEIVE_SEARCH_RESULTS,
        payload: {
            key: key,
            data:data
        }
    }
}

