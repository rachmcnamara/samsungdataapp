/**
 * Created by rachelmcnamara on 4/4/17.
 */
export const SET_CURRENT_TAB = "setCurrentTab"
export const SET_CURRENT_URL = "setCurrentURL"

export function setCurrentTab(index)
{
  return {
    type:SET_CURRENT_TAB,
    payload:{
      index:index
    }
  }
}

export function setCurrentURL(url)
{
  return {
    type:SET_CURRENT_URL,
    payload:{
      url:url
    }
  }
}