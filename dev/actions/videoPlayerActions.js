/**
 * Created by velopedev on 4/17/17.
 */

export const PLAY_VIDEO = "playVideo"
export const PAUSE_VIDEO = "pauseVideo"
export const MUTE_VIDEO = "muteVideo"
export const FF_VIDEO = "ff"
export const REWIND_VIDEO = "rewind"

export function playVideo()
{
    return {
        type:PLAY_VIDEO,
        payload:{
            isPlaying:true
        }
    }
}

export function pauseVideo()
{
    return {
        type:PAUSE_VIDEO,
        payload:{
            isPlaying:false
        }
    }
}

export function muteVideo(isMuted)
{
    return {
        type:MUTE_VIDEO,
        payload:{
            isMuted:isMuted
        }
    }
}

export function rewindVideo(rate)
{
    return {
        type:REWIND_VIDEO,
        payload:{
            isRewinding:true,
            rate:rate
        }
    }
}

export function ffVideo(rate)
{
    return {
        type:FF_VIDEO,
        payload:{
            isFF:true,
            rate:rate
        }
    }
}

