/**
 * Created by rachelmcnamara on 3/28/17.
 */
import rootReducer from './reducers'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import {createLogger} from 'redux-logger'
import createHistory from 'history/createBrowserHistory'
import {routerReducer, routerMiddleware } from 'react-router-redux'

const loggerMiddleware = createLogger()
const history =  createHistory()

// Build the middleware for intercepting and dispatching navigation actions
const routerMid = routerMiddleware(history)

export default function configureStore(initialState) {
  return createStore(
      rootReducer,
      initialState,
      routerReducer,
      applyMiddleware(
          thunkMiddleware,
          loggerMiddleware,
          routerMid
      )
  )
}