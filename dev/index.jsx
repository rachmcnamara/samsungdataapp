/**
 * Created by rachelmcnamara on 3/17/17.
 */
import 'babel-polyfill'
import React from "react"
import { render } from 'react-dom'
import Root from './containers/Root'

render(
    <Root />,
    document.getElementById('container')
)